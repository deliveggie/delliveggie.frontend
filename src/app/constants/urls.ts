import {environment} from '../../environments/environment';

export const urls = {
  productUrl : environment.apiUrl + 'Products/'
};
