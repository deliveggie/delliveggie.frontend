import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProductDetailsModel} from '../models/product-details-model';
import {ProductsService} from '../services/products-service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {

  product: ProductDetailsModel;
  productId: string | null;
  constructor(private route: ActivatedRoute, private service: ProductsService) {
    this.productId = '';
    this.product = new ProductDetailsModel();
  }

  ngOnInit(): void {
    this.productId = this.route.snapshot.paramMap.get('id');
    if (this.productId != null) {
      this.service.fetchProductDetails(this.productId as string).subscribe((product: ProductDetailsModel ) => {
        return this.product = product;
      });
    }
  }

  getProductCurrency(): string{
    return new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(this.product.priceWithReduction);
}


}
