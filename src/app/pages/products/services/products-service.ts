import {HttpClient} from '@angular/common/http';
import {ProductModel} from '../models/product-model';
import {Observable} from 'rxjs';
import {urls} from '../../../constants/urls';
import {Injectable} from '@angular/core';
import {ProductDetailsModel} from '../models/product-details-model';

@Injectable()
export class ProductsService {
  constructor(private http: HttpClient) { }

  public fetchProducts(): Observable<ProductModel[]> {
    return this.http
      .get<any>(urls.productUrl + 'GetProducts');
  }

  public fetchProductDetails(id: string | null): Observable<ProductDetailsModel> {
    return this.http
      .get<any>(urls.productUrl + 'GetProductDetails?id=' + id);
  }
}
