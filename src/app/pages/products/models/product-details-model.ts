export  class ProductDetailsModel{
  // @ts-ignore
  id: string;
  // @ts-ignore
  name: string;
  // @ts-ignore
  entryDate: Date;
  // @ts-ignore
  priceWithReduction: number;
}
