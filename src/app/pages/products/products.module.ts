import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductDetailsComponent } from './product-details/product-details.component';
import {ProductsService} from './services/products-service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {ProductsViewComponent} from './products-view/products-view.component';

@NgModule({
  declarations: [ProductDetailsComponent],
  imports: [
    RouterModule.forChild([
      {path: 'products', component: ProductsViewComponent},
      {path: 'products/:id', component: ProductDetailsComponent}
    ]),
    CommonModule,
    HttpClientModule
  ],
  providers: [ProductsService, HttpClient]
})
export class ProductsModule { }
