import { Component, OnInit } from '@angular/core';
import {ProductsService} from '../services/products-service';
import {ProductModel} from '../models/product-model';

@Component({
  selector: 'app-products-view',
  templateUrl: './products-view.component.html',
  styleUrls: ['./products-view.component.scss']
})
export class ProductsViewComponent implements OnInit {

  products: ProductModel[];

  constructor(private service: ProductsService) {
    this.products = [];
  }

  ngOnInit(): void {
    this.service.fetchProducts().subscribe((products: ProductModel[] ) => {
      return this.products = products;
    });
  }
}
