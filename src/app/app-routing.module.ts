import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ProductsViewComponent} from './pages/products/products-view/products-view.component';
import {ProductDetailsComponent} from './pages/products/product-details/product-details.component';

export const routes: Routes = [
  {path: '', redirectTo: '/products', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
